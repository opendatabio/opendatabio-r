# Opendatabio R package

This package implements a client to an [OpenDataBio API](https://opendatabio.gitlab.io/en/docs/api/). This should allow for users with some experience in R to interact with an OpenDataBio database to export (GET), import (POST) and update (PUT) data.

The package is written on top of [httr](http://httr.r-lib.org/) package. Advanced users may just write their own requests using the `httr::GET` and `httr::POST` functions directly.

## Install

Installation is currently done only from gitlab. Package `devtools` required.

```R
library(devtools)
install_gitlab(repo="opendatabio/opendatabio-r",dependencies = "Imports")
#to use
library(opendatabio)
```

## Basic usage

1. [OpenDataBio GET Data Tutorial](https://opendatabio.gitlab.io/en/docs/tutorials/01-get-r-vignette/)
1. [OpenDataBio POST Data Tutorial](https://opendatabio.gitlab.io/en/docs/tutorials/02-post-data-r-vignette/).

# Note to package developers!
Note that running `devtools::check()` or `devtools::test()` will overwrite your environment variables!

# License

The documentation site is licensed for use under a GPLv3 license. See the  LICENSE file provided.
