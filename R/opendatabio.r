#' Client for OpenDataBio APIs
#'
#' This package provides functions for interacting with the web services (API) of \href{https://opendatabio.gitlab.io/docs/api}{OpenDataBio} databases, for downloading, importing, and updating data. 
#' 
#' \itemize{
#'   \item \link[=odb_config]{Connect to an OpenDataBio server}
#'   \item \link[=odb_get]{Download Data}
#'   \item \link[=odb_import_individuals]{Import New Data}
#'   \item \link[=odb_update_individuals]{Update Existing Data}
#'   \item \link[=odb_get_jobs]{Monitor Submitted Jobs}
#'   \item \link[=odb_taxonLevelCodes]{Helpers}
#'   \item \href{https://opendatabio.gitlab.io/docs/api}{OpenDataBio API docs}
#' }
#' 
#' _PACKAGE
#' @name opendatabio
NULL
#> NULL
